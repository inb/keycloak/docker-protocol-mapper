/*
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.bsc.inb.elixir.openebench.keycloak.protocol.docker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.keycloak.jose.jws.JWSBuilder;
import org.keycloak.models.AuthenticatedClientSessionModel;
import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeyManager;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.docker.DockerAuthV2Protocol;
import org.keycloak.protocol.docker.DockerKeyIdentifier;
import org.keycloak.protocol.docker.mapper.DockerAuthV2AttributeMapper;
import org.keycloak.protocol.docker.mapper.DockerAuthV2ProtocolMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenResponseMapper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.docker.DockerAccess;
import org.keycloak.representations.docker.DockerResponseToken;

/**
 * @author Dmitry Repchevsky
 */

public class DockerUserAttributeProtocolMapper extends DockerAuthV2ProtocolMapper 
        implements DockerAuthV2AttributeMapper, OIDCAccessTokenMapper, OIDCAccessTokenResponseMapper {

    public static final String PATTERN_PROPERTY_NAME = "pattern";
    public static final String PATTERN_PROPERTY_LABEL = "Namespace matching";
    public static final String PATTERN_PROPERTY_TOOLTIP = "Check docker namespace matching user attribute in a form '$attr1/$attr2/...'.";
    public static final String PROVIDER_ID = "docker-v2-user-attribute-mapper";
    
    private final List<ProviderConfigProperty> configProperties;

    public DockerUserAttributeProtocolMapper() {
        configProperties = new ArrayList<>();
        
        final ProviderConfigProperty providerConfigProperty = 
                new ProviderConfigProperty(
                        PATTERN_PROPERTY_NAME, 
                        PATTERN_PROPERTY_LABEL, 
                        PATTERN_PROPERTY_TOOLTIP,
                        ProviderConfigProperty.STRING_TYPE, 
                        null);

        configProperties.add(providerConfigProperty);
    }
    
    @Override
    public String getDisplayType() {
        return "Matching User Attributes";
    }

    @Override
    public String getHelpText() {
        return "Allows grants matching docker namespace with user attributes";
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public boolean appliesTo(final DockerResponseToken responseToken) {
        return true;
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public DockerResponseToken transformDockerResponseToken(final DockerResponseToken responseToken, final ProtocolMapperModel mappingModel,
            final KeycloakSession session, final UserSessionModel userSession, final AuthenticatedClientSessionModel clientSession) {

        responseToken.getAccessItems().clear();

        final String access = transformResponseToken(mappingModel, session, userSession, clientSession);
        if (access != null && !access.isEmpty()) {
            responseToken.getAccessItems().add(new DockerAccess(access));
        }
        return responseToken;
    }

    @Override
    public AccessToken transformAccessToken(AccessToken accessToken, ProtocolMapperModel mappingModel, KeycloakSession session, UserSessionModel userSession, ClientSessionContext clientSessionCtx) {
        accessToken.getOtherClaims().remove("access");
        
        final String access = transformResponseToken(mappingModel, 
                session, userSession, clientSessionCtx.getClientSession());
        if (access != null && !access.isEmpty()) {
            accessToken.getOtherClaims().put("access", Arrays.asList(new DockerAccess(access)));
        }

        accessToken.setSubject(userSession.getUser().getUsername());
        
        return accessToken.issuedFor(clientSessionCtx.getClientSession().getClient().getClientId());
    }
    
    /**
     * The hack to resign access token with docker 'kid'
     * 
     * @param accessTokenResponse
     * @param mappingModel
     * @param session
     * @param userSession
     * @param clientSessionCtx
     * @return 
     */
    @Override
    public AccessTokenResponse transformAccessTokenResponse(AccessTokenResponse accessTokenResponse, 
            ProtocolMapperModel mappingModel, KeycloakSession session, UserSessionModel userSession, ClientSessionContext clientSessionCtx) {
        
        final AccessToken accessToken = session.tokens().decode(accessTokenResponse.getToken(), AccessToken.class);
        
        try {
            final KeyManager.ActiveRsaKey activeKey = session.keys().getActiveRsaKey(userSession.getRealm());
            final String encodedToken = new JWSBuilder()
                    .kid(new DockerKeyIdentifier(activeKey.getPublicKey()).toString())
                    .type("JWT")
                    .jsonContent(accessToken)
                    .rsa256(activeKey.getPrivateKey());
            accessTokenResponse.setToken(encodedToken);
        } catch (final InstantiationException e) {
        }
        
        return accessTokenResponse;
    }

    private String transformResponseToken(final ProtocolMapperModel mappingModel,
            final KeycloakSession session, final UserSessionModel userSession, final AuthenticatedClientSessionModel clientSession) {

        final String requestedScope = clientSession.getNote(DockerAuthV2Protocol.SCOPE_PARAM);
        if (requestedScope != null) {
            final String patterns = mappingModel.getConfig().getOrDefault(PATTERN_PROPERTY_NAME, "");
            if (!patterns.isEmpty()) {
                final DockerAccess docker_access = new DockerAccess(requestedScope);
                for (String pattern : patterns.split(",")) {
                    final DockerAccess pattern_access = new DockerAccess(pattern.trim());
                    if (docker_access.getType().equals(pattern_access.getType())) {
                        final String[] pattern_parts = pattern_access.getName().split("/");
                        final String[] namespace_parts = docker_access.getName().split("/");

                        if (namespace_parts.length >= pattern_parts.length) {
                            final UserModel userModel = userSession.getUser();
                            label:
                            for (int i = 0; i < pattern_parts.length; i++) {
                                if (!pattern_parts[i].isEmpty()) {
                                    if (pattern_parts[i].charAt(0) == '$' && pattern_parts[i].length() > 1) {
                                        final String attr = pattern_parts[i].substring(1);
                                        final List<String> attributes = userModel.getAttribute(attr);
                                        if (attributes != null && attributes.contains(namespace_parts[i])) {
                                            continue;
                                        }
                                        final Set<GroupModel> groups = userModel.getGroups();
                                        if (groups != null) {
                                            for (GroupModel group : groups) {
                                                final List<String> gattributes = group.getAttribute(attr);
                                                if (gattributes != null && gattributes.contains(namespace_parts[i])) {
                                                    continue label;
                                                }
                                            }
                                        }
                                    } else if (pattern_parts[i].equals(namespace_parts[i])) {
                                        continue; // match to const
                                    }
                                    return null; // no match
                                }
                            }

                            final List<String> pattern_actions = pattern_access.getActions();
                            if (!pattern_actions.contains("*")) {
                                for (String docker_action : docker_access.getActions()) {
                                    if (!pattern_actions.contains(docker_action)) {
                                        return null;
                                    }
                                }
                            }
                            return requestedScope;
                        }
                    }
                }
            }
        }

        return null;
    }
}
