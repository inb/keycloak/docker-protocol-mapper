###  OpenEBench Keycloak Docker Authorization Module

###### Keycloak Docker Protocol Mapper
The mapper is implemented as a WildFly module and performs user authorization
via matching docker namespace with a Keycloak user's attributes.  
The configuration has only one parameter: "Namespace matching" which is a string that
defines the docker namespace matching pattern, for example "registry:catalog:GET, registry:$username:*".
Note that attriutes (i.e. 'username') are looked for the authenticated user as well as in 
all groups this user belongs to.
###### Apache Maven build system
To simplify build process the module uses [Apache Maven](https://maven.apache.org/) build system.

To build the module:

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/keycloak/docker-protocol-mapper.git
>cd docker-protocol-mapper
>mvn install
```

The latter generates ./target/docker-protocol-mapper-1.0.0.jar library

###### Keycloak integration
Generated **docker-protocol-mapper-1.0.0.jar** file must be put into the Keycloak mudules directory.
To the same place **module.xml** must be put:
```
$KEYCLOAK_HOME/modules/system/layers/keycloak/es/bsc/inb/elixir/docker-protocol-mapper/main/docker-protocol-mapper-1.0.0.jar
$KEYCLOAK_HOME/modules/system/layers/keycloak/es/bsc/inb/elixir/docker-protocol-mapper/main/module.xml
```
In addition, Keycloak must be configured to load this module. 
In the Keycloak ./standalone/configuration/standalone.xml config file add the provider:
```xml
<subsystem xmlns="urn:jboss:domain:keycloak-server:1.1">
    <providers>
        <provider>classpath:${jboss.home.dir}/providers/*</provider>
        <provider>module:es.bsc.inb.elixir.docker-protocol-mapper</provider>
    </providers>
```
